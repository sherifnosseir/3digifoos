int row[8] = {
  52, 42, 53, 46, 39, 51, 41, 47
};

int col[8] = {
  44, 43, 45, 50, 49, 48, 40, 38
};

// " "
static const int SPACE[8][8] {{0, 0, 0, 0, 0, 0, 0, 0}, \
                              {0, 0, 0, 0, 0, 0, 0, 0}, \
                              {0, 0, 0, 0, 0, 0, 0, 0}, \
                              {0, 0, 0, 0, 0, 0, 0, 0}, \
                              {0, 0, 0, 0, 0, 0, 0, 0}, \
                              {0, 0, 0, 0, 0, 0, 0, 0}, \
                              {0, 0, 0, 0, 0, 0, 0, 0}, \
                              {0, 0, 0, 0, 0, 0, 0, 0}};


//0
static const int LETTHEFUNBEGIN[8][8] {{0, 0, 1, 1, 1, 1, 0, 0}, \
                                       {0, 1, 0, 0, 0, 0, 1, 0}, \
                                       {0, 1, 0, 0, 0, 1, 1, 0}, \
                                       {0, 1, 0, 0, 1, 0, 1, 0}, \
                                       {0, 1, 0, 1, 0, 0, 1, 0}, \
                                       {0, 1, 1, 0, 0, 0, 1, 0}, \
                                       {0, 1, 0, 0, 0, 0, 1, 0}, \
                                       {0, 0, 1, 1, 1, 1, 0, 0}};

//1
 static const int UNO[8][8] {{0, 0, 0, 0, 1, 0, 0, 0}, \
                             {0, 0, 0, 1, 1, 0, 0, 0}, \
                             {0, 0, 1, 0, 1, 0, 0, 0}, \
                             {0, 0, 0, 0, 1, 0, 0, 0}, \
                             {0, 0, 0, 0, 1, 0, 0, 0}, \
                             {0, 0, 0, 0, 1, 0, 0, 0}, \
                             {0, 0, 0, 0, 1, 0, 0, 0}, \
                             {0, 0, 1, 1, 1, 1, 1, 0}};


//2
 static const int DOS[8][8] {{0, 0, 1, 1, 1, 1, 0, 0}, \
                             {0, 1, 0, 0, 0, 0, 1, 0}, \
                             {0, 0, 0, 0, 0, 0, 1, 0}, \
                             {0, 0, 0, 0, 0, 1, 0, 0}, \
                             {0, 0, 0, 1, 1, 0, 0, 0}, \
                             {0, 0, 1, 0, 0, 0, 0, 0}, \
                             {0, 1, 0, 0, 0, 0, 0, 0}, \
                             {0, 1, 1, 1, 1, 1, 1, 0}};

//3
static const int TRES[8][8] {{0, 0, 1, 1, 1, 1, 0, 0}, \
                             {0, 1, 0, 0, 0, 0, 1, 0}, \
                             {0, 0, 0, 0, 0, 0, 1, 0}, \
                             {0, 0, 0, 1, 1, 1, 0, 0}, \
                             {0, 0, 0, 0, 0, 0, 1, 0}, \
                             {0, 0, 0, 0, 0, 0, 1, 0}, \
                             {0, 1, 0, 0, 0, 0, 1, 0}, \
                             {0, 0, 1, 1, 1, 1, 0, 0}};

//4
static const int CUATRO[8][8] {{0, 0, 0, 0, 0, 1, 0, 0}, \
                               {0, 0, 0, 0, 1, 1, 0, 0}, \
                               {0, 0, 0, 1, 0, 1, 0, 0}, \
                               {0, 0, 1, 0, 0, 1, 0, 0}, \
                               {0, 1, 0, 0, 0, 1, 0, 0}, \
                               {0, 1, 1, 1, 1, 1, 1, 0}, \
                               {0, 0, 0, 0, 0, 1, 0, 0}, \
                               {0, 0, 0, 0, 0, 1, 0, 0}};

//5
static const int CINCO[8][8] {{0, 1, 1, 1, 1, 1, 1, 0}, \
                              {0, 1, 0, 0, 0, 0, 0, 0}, \
                              {0, 1, 0, 0, 0, 0, 0, 0}, \
                              {0, 1, 1, 1, 1, 1, 0, 0}, \
                              {0, 0, 0, 0, 0, 0, 1, 0}, \
                              {0, 0, 0, 0, 0, 0, 1, 0}, \
                              {0, 1, 0, 0, 0, 0, 1, 0}, \
                              {0, 0, 1, 1, 1, 1, 0, 0}};

//6
static const int SEIS[8][8] {{0, 0, 1, 1, 1, 1, 0, 0}, \
                             {0, 1, 0, 0, 0, 0, 1, 0}, \
                             {0, 1, 0, 0, 0, 0, 0, 0}, \
                             {0, 1, 1, 1, 1, 1, 0, 0}, \
                             {0, 1, 0, 0, 0, 0, 1, 0}, \
                             {0, 1, 0, 0, 0, 0, 1, 0}, \
                             {0, 1, 0, 0, 0, 0, 1, 0}, \
                             {0, 0, 1, 1, 1, 1, 0, 0}};

//7
static const int SIETE[8][8] {{0, 1, 1, 1, 1, 1, 1, 0}, \
                              {0, 0, 0, 0, 0, 0, 1, 0}, \
                              {0, 0, 0, 0, 0, 0, 1, 0}, \
                              {0, 0, 0, 0, 0, 1, 0, 0}, \
                              {0, 0, 0, 0, 1, 0, 0, 0}, \
                              {0, 0, 0, 1, 0, 0, 0, 0}, \
                              {0, 0, 1, 0, 0, 0, 0, 0}, \
                              {0, 1, 0, 0, 0, 0, 0, 0}};

//8
static const int OCHO[8][8] {{0, 0, 1, 1, 1, 1, 0, 0}, \
                             {0, 1, 0, 0, 0, 0, 1, 0}, \
                             {0, 1, 0, 0, 0, 0, 1, 0}, \
                             {0, 0, 1, 1, 1, 1, 0, 0}, \
                             {0, 1, 0, 0, 0, 0, 1, 0}, \
                             {0, 1, 0, 0, 0, 0, 1, 0}, \
                             {0, 1, 0, 0, 0, 0, 1, 0}, \
                             {0, 0, 1, 1, 1, 1, 0, 0}};

//9
static const int NEUVE[8][8] {{0, 1, 1, 1, 1, 1, 1, 0}, \
                              {0, 1, 0, 0, 0, 0, 1, 0}, \
                              {0, 1, 0, 0, 0, 0, 1, 0}, \
                              {0, 1, 0, 0, 0, 0, 1, 0}, \
                              {0, 1, 1, 1, 1, 1, 1, 0}, \
                              {0, 0, 0, 0, 0, 0, 1, 0}, \
                              {0, 0, 0, 0, 0, 0, 1, 0}, \
                              {0, 1, 1, 1, 1, 1, 1, 0}};


// update as more numbers get added
int getNumbersCount() {
  return 10; // 0 --> N-1
}
// takes a number, and returns the requested row and col
int getNumber(int number, int row, int col) {
  switch (number) {
    case 0:
      return LETTHEFUNBEGIN[row][col];
      break;
    case 1:
      return UNO[row][col];
      break;
    case 2:
      return DOS[row][col];
      break;
    case 3:
      return TRES[row][col];
      break;
    case 4:
      return CUATRO[row][col];
      break;
    case 5:
      return CINCO[row][col];
      break;
    case 6:
      return SEIS[row][col];
      break;
    case 7:
      return SIETE[row][col];
      break;
    case 8:
      return OCHO[row][col];
      break;
    case 9:
      return NEUVE[row][col];
      break;
  }
}

//
//
// END OF DEFINING NUMBERS
//


static const int ROWS = 8, COLS = 8;

//sensors
//
const int incrSensor = 9;
const int decrSensor = 8;

//buttons
//
const int resetButton = A8;
//int resetButtonVoltage = LOW;

const int incrButton = A0;
//int incrButtonVoltage = LOW;

const int decrButton = A1;
//int decrButtonVoltage = LOW;

//END OF BUTTONS

//sensor states
volatile int incrSensorLastState = HIGH;
volatile int incrSensorCurrentState = incrSensorLastState;
volatile int decrSensorLastState = HIGH;
volatile int decrSensorCurrentState = decrSensorLastState;

void setup() {
  // put your setup code here, to run once:
for (int thisPin = 0; thisPin < 8; thisPin++) {
    // initialize the output pins:
    pinMode(col[thisPin], OUTPUT);
    pinMode(row[thisPin], OUTPUT);

    pinMode (incrSensor, INPUT);
    pinMode (decrSensor, INPUT);

    pinMode(resetButton, INPUT);

    Serial.begin(9600);
    // take the col pins (i.e. the cathodes) high to ensure that
    // the LEDS are off:
  }

  delay(1000);
  for (int thisPin = 0; thisPin < 8; thisPin++) {
    
    digitalWrite(row[thisPin], LOW);
    digitalWrite(col[thisPin], HIGH);
  }
  delay(1000);

  Reset();
}

int LED1 = 0; // TELL LED 1 WHAT TO DISPLAY
int current = HIGH;


// THIS SHOULD ONLY TAKE: 
// incrSensor 
// decrSensor
// resetButton
// change as needed
boolean checkSensorWithPinNumber(int pinNumber) {
  boolean change = false;
  int curVoltage = 0;

  switch(pinNumber) {
    case incrSensor:
      incrSensorCurrentState = digitalRead(pinNumber);
      if (incrSensorCurrentState != incrSensorLastState && incrSensorCurrentState == LOW) {
        change = true;
        delay(2);
      }
      incrSensorLastState = incrSensorCurrentState;
      break;
   case decrSensor:
     decrSensorCurrentState = digitalRead(pinNumber);
     if (decrSensorCurrentState != decrSensorLastState && decrSensorCurrentState == LOW) {
        change = true;
        delay(2);
      }
      decrSensorLastState = decrSensorCurrentState;
     break;
   case incrButton:
     curVoltage = analogRead(incrButton);
     curVoltage = (curVoltage >= 1000)? HIGH : LOW;
  
     if (curVoltage == HIGH) {
      change = true;
      delay(250);
     }
//     incrButtonVoltage = curVoltage;
     break;
   case decrButton:
     curVoltage = analogRead(decrButton);
     curVoltage = (curVoltage >= 1000)? HIGH : LOW;
  
     if (curVoltage == HIGH) {
      change = true;
      delay(250);
     }
//     decrButtonVoltage = curVoltage;
     break;
   case resetButton:
     curVoltage = analogRead(resetButton);
     curVoltage = (curVoltage >= 1000)? HIGH : LOW;
  
     if (curVoltage == HIGH) {
      change = true;
      delay(750);
     }
//     resetButtonVoltage = curVoltage;
     break;
  }    
  
  return change;
}

//check for input
void Check() {
  if (LED1 < 10){
//  increment score button has been pressed 
    if (checkSensorWithPinNumber(incrSensor) || checkSensorWithPinNumber(incrButton)) {
      LED1 = (LED1 >= 9)? 9 : LED1+1;
    }

//  decrement score button has been pressed
    if (checkSensorWithPinNumber(decrSensor) || checkSensorWithPinNumber(decrButton)) {
      LED1 = (LED1 <= 0)? 0 : LED1-1;
    }
  }

//  reset button has been pressed
  if (checkSensorWithPinNumber(resetButton)) {
    Reset();
  }
}


int fps = 64;
int scrollerFPS = 4;
int frame = 0;
int displayOffset = 0;

int sideScroller = 0;
int scrollerState = 1; // 0 for up, 1 for down

int GoalCounter = 0;

void Update() {
// if (frame%fps == 0) {
//  if (displayOffset < 25) {
//    LED1 = (LED1+1)%getNumbersCount(); 
//  } 
  
//  displayOffset = (displayOffset+1)%10;

//  scrollerState = (scrollerState==0)? 1 : 0;

// }
  if (GoalCounter > 5) {
    if (GoalCounter == 9) {
      scrollerFPS = 1;
    } else if (GoalCounter < 8) {
      scrollerFPS = 2;
    } else {
      scrollerFPS = 3;
    }
  } else {
    scrollerFPS = 5-GoalCounter;
  }

//  Serial.print
  
  if (frame%scrollerFPS == 0) {
    sideScroller = (sideScroller+1)%ROWS; 
  }
  frame = (frame+1)%fps;
}

boolean DrawRowAnimation(int r, int c) {
  int calculatedRow = (scrollerState == 0)? (ROWS-1-r) : r;
  
       if (calculatedRow == sideScroller) {
        digitalWrite(col[c], LOW);
        return true;
       } else {
        digitalWrite(col[c], HIGH);
        return false;
       }
}

void Draw(int LED) {
  for (int r = 0; r < ROWS; r++) {

      digitalWrite(row[r], HIGH);
  
      for (int c = 0; c < COLS; c++) {

        if (c == 0 || c == COLS-1) {
          DrawRowAnimation(r, c);
        } else if (getNumber(LED, r, c) == 0) {
//        int number[8][8] = numbers[i];
          digitalWrite(col[c], HIGH);
        } else {
          digitalWrite(col[c], LOW);
        }
      }
    
      delay(2);
      digitalWrite(row[r], LOW);

      if (r == 0) {
        scrollerState = (scrollerState==0)? 1 : 0;
      }
      if (r == ROWS) {
        scrollerState = (scrollerState==1)? 0 : 1;
      }
    }
}

void Reset() {
  scrollerFPS = 4;
  frame = 0;
  displayOffset = 0;
  GoalCounter = 0;
}

void loop() {
  // put your main code here, to run repeatedly:
  Check();
  Update();
  Draw(LED1);
}
